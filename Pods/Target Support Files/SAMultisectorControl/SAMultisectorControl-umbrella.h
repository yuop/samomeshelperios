#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "SAMath.h"
#import "SAMultisectorControl.h"

FOUNDATION_EXPORT double SAMultisectorControlVersionNumber;
FOUNDATION_EXPORT const unsigned char SAMultisectorControlVersionString[];

