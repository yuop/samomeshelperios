//
//  APIManager.swift
//  SamomesHelper
//
//  Created by Savelyev Yura on 19/11/2016.
//  Copyright © 2016 Savelyev Yura. All rights reserved.
//

import Foundation
import SwiftyJSON
class APIManager {
    
    static let sharedInstance = APIManager()
    static let baseProtocol = "http"
    static let baseDomain = "176.112.219.109"
    static let baseFile = "vape/liquidAPI.php"
    
    fileprivate let baseURL = "\(baseProtocol)://\(baseDomain)/\(baseFile)"
    
    
    private func makeHTTPRequestWithData(method: String, route: String,body: [String:Any],onCompletion: @escaping (JSON) -> Void, onFail: @escaping (JSON) -> Void) {
        let path = route.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = URLRequest(url: URL(string: path!)!)
        let session = URLSession.shared
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = method
        request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        let task = session.dataTask(with: request, completionHandler: {data,response,error in
            if let _response = response as? HTTPURLResponse {
                var json = JSON(["error":"error occured"])
                if let _data = data {
                    json = JSON(_data)
                }
                if _response.statusCode == 200 {
                        onCompletion(json)
                } else {
                        onFail(json)
                }
            }
        })
    }
}
