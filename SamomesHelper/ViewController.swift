//
//  ViewController.swift
//  SamomesHelper
//
//  Created by Savelyev Yura on 26/05/16.
//  Copyright © 2016 Savelyev Yura. All rights reserved.
//
import UIKit
import SAMultisectorControl
import GoogleSignIn
//import Appodeal

class ViewController: UIViewController {
    @IBOutlet weak var volume: UILabel!
    @IBOutlet weak var segmenter: UISegmentedControl!
    @IBOutlet weak var multiselectorControl: SAMultisectorControl!
    
    @IBOutlet weak var tintEffectView: UIVisualEffectView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var themeButton: UIButton!
    
    
    @IBOutlet weak var vgDrips: UILabel!
    @IBOutlet weak var vgMg: UILabel!
    
    @IBOutlet weak var pgDrips: UILabel!
    @IBOutlet weak var pgMg: UILabel!
    
    @IBOutlet weak var aromaDrips: UILabel!
    @IBOutlet weak var aromaMg: UILabel!
    
    @IBOutlet weak var nicDrips: UILabel!
    @IBOutlet weak var nicMg: UILabel!
    @IBOutlet weak var labelNic: UILabel!
    var googleButton = GIDSignInButton(frame: CGRect(origin: CGPoint(x: 20, y: 20), size: CGSize(width: 60, height: 40)))
    let redColor = UIColor(red: 212, green: 0, blue: 0, alpha: 1)
    let greenColor = UIColor(red:  0, green: 199, blue: 0, alpha: 1)
    let blueColor = UIColor(red: 0, green: 168, blue: 255, alpha: 1)
    let whiteColor = UIColor(red: 20, green: 120, blue: 241, alpha: 1)
    var swipeGesture: UISwipeGestureRecognizer!
    var defaultValues = [10.0,30.0,50.0,100.0]
    var newValues = [120.0,180.0,250.0,500.0]
    
    fileprivate var appWasUnhidden = 0
    fileprivate var minimumSessions = 2
    fileprivate var tryAgaingAfter_Launches = 2
    
    var isDefaultValues = true
    var volumeLiquid = 0.0
    
    @IBAction func tutorialTap(_ sender: AnyObject) {
        UserDefaults.standard.set(true, forKey: "notNewLaunch")
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .transitionFlipFromLeft, animations: {
            self.tintEffectView.alpha = 0
            }, completion: { _ in
                self.tintEffectView.isHidden = true
        })
    }
    @IBAction func themeTap(_ sender: UIButton) {
        if view.backgroundColor == UIColor.black {
            sender.backgroundColor = UIColor.black
            view.backgroundColor = UIColor.darkGray
            segmenter.backgroundColor = UIColor.darkGray
        } else {
            sender.backgroundColor = UIColor.darkGray
            view.backgroundColor = UIColor.black
            segmenter.backgroundColor = UIColor.black
            
        }
        
        
        
    }
    func swipeAction(){
        if isDefaultValues {
            print("swiped")
            swipeGesture.direction = .right
            isDefaultValues = false
            UIView.animate(withDuration: 0.5, animations: {
                for index in 0..<self.newValues.count {
                    self.segmenter.setTitle("\(Int(self.newValues[index]))мл", forSegmentAt: index)
                }
            })
            self.segmenter.selectedSegmentIndex = 0
            valueChanged(segmenter)
        } else {
            swipeGesture.direction = .left
            isDefaultValues = true
            UIView.animate(withDuration: 0.5, animations: {
                for index in 0..<self.defaultValues.count {
                    self.segmenter.setTitle("\(Int(self.defaultValues[index]))мл", forSegmentAt: index)
                }
            })
            
            self.segmenter.selectedSegmentIndex = 0
            valueChanged(segmenter)
            
        }
        
    }
    
    func deleteLettersFromString(_ myString: String) -> Double {
        return Double(myString.replacingOccurrences(of: "мл", with: ""))!
    }
    
    @IBAction func valueChanged(_ sender: UISegmentedControl) {

        volumeLiquid = deleteLettersFromString(sender.titleForSegment(at: sender.selectedSegmentIndex)!)
        updateDataView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.addSubview(googleButton)
//        googleButton.style = .iconOnly
//        GIDSignIn.sharedInstance().uiDelegate = self
        // setup ViewController
        
        setupDesign()
        setupMultiselectorControl()
        volumeLiquid = 10
        updateDataView()
        if UserDefaults.standard.bool(forKey: "notNewLaunch"){
                Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.showRateAlert), userInfo: nil, repeats: false)
        }
        
        swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeAction))
        swipeGesture.direction = .left
        segmenter.addGestureRecognizer(swipeGesture)
        
        themeButton.layer.cornerRadius = 10
        themeButton.layer.borderWidth = 1
        themeButton.layer.borderColor = UIColor(red: 1, green: 1, blue: 11/255, alpha: 1).cgColor
        themeButton.backgroundColor = UIColor.lightGray
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !UserDefaults.standard.bool(forKey: "notNewLaunch") {
            UIView.animate(withDuration: 1, delay: 0.0, options: .transitionFlipFromLeft, animations: {
                self.doneButton.layer.borderColor = UIColor.white.cgColor
                self.doneButton.layer.cornerRadius = 5
                self.doneButton.layer.borderWidth = 1
                self.tintEffectView.isHidden = false
                self.tintEffectView.alpha = 1
                }, completion: nil)
        }

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDesign() {
        self.view.backgroundColor = UIColor.black
        let normalAttr : [AnyHashable: Any] = [NSForegroundColorAttributeName : UIColor.yellow, NSFontAttributeName : UIFont(name: "Raleway", size: 13)!]
        let selectedAttr : [AnyHashable: Any] = [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName : UIFont(name: "Raleway", size: 13)!]
        segmenter.setTitleTextAttributes(normalAttr as [AnyHashable: Any], for: UIControlState())
        segmenter.setTitleTextAttributes(selectedAttr, for: .selected)
        
    }
    
    
    func setupMultiselectorControl () {
        print("sMC")
        self.multiselectorControl.addTarget(self, action: #selector(self.updateDataView), for: .valueChanged)
        
        //creating sectors for calc
        let sectorVG = SAMultisectorSector(color: greenColor, maxValue: 100)
        let sectorPG = SAMultisectorSector(color: blueColor, maxValue: 100)
        let sectorAroma = SAMultisectorSector(color: redColor, maxValue: 35)
        let sectorNic = SAMultisectorSector(color: whiteColor, maxValue: 36)
        
        sectorNic?.tag = 0
        sectorAroma?.tag = 1
        sectorPG?.tag = 2
        sectorVG?.tag = 3
        
        sectorVG?.endValue = 80
        sectorPG?.endValue = 20
        sectorAroma?.endValue = 20
        sectorNic?.endValue = 3
        
        self.multiselectorControl.addSector(sectorNic)
        self.multiselectorControl.addSector(sectorAroma)
        self.multiselectorControl.addSector(sectorPG)
        self.multiselectorControl.addSector(sectorVG)
        
        
    }
    
    
    func updateDataView() {
        
        var dummyVolume = volumeLiquid
        
        let aromaSector = multiselectorControl.sectors[1] as! SAMultisectorSector
        let aromMgs = volumeLiquid * Double(round(aromaSector.endValue) / 100)
        let aromDrips = volumeLiquid * Double(round(aromaSector.endValue) / 5)
        
        dummyVolume -= aromMgs
        aromaMg.text = NSString(format: "%.1f мл", aromMgs) as String
        aromaDrips.text = "\(Int(aromDrips))"
        
        let nicSector = multiselectorControl.sectors[0] as! SAMultisectorSector
        let nicMgs = volumeLiquid / 100 * Double(round(nicSector.endValue))
        let nicDrip = nicMgs * 20
        
        dummyVolume -= nicMgs
        nicMg.text = NSString(format: "%.1f мл", nicMgs) as String
        nicDrips.text = "\(Int(nicDrip))"
        
        let vgSector = multiselectorControl.sectors[3] as! SAMultisectorSector
        let vgMgs = dummyVolume * Double(round(vgSector.endValue)/100)
        let vgDrip = vgMgs * 20
        vgSector.isRight = true
        
        
        
        vgMg.text = NSString(format: "%.2f мл", vgMgs) as String
        vgDrips.text = "\(Int(vgDrip))"
        
        let pgSector = multiselectorControl.sectors[2] as! SAMultisectorSector
        let pgMgs = dummyVolume * Double(round(pgSector.endValue)/100)
        let pgDrip = pgMgs * 20
        
        pgMg.text = NSString(format: "%.2f мл", pgMgs) as String
        pgDrips.text = "\(Int(pgDrip))"
        
        print("\(vgSector.isTouched) vg")
        print("\(pgSector.isTouched) pg")
        // covariation for PG and VG sectors
        if vgSector.isTouched {
            pgSector.endValue = 100 - Double(round(vgSector.endValue))
            
            
        } else if pgSector.isTouched {
            vgSector.isTouched = false
            vgSector.endValue = 100 - Double(round(pgSector.endValue))
            vgSector.isRight = true
        }

    }
    
//Ads
/*   func showAd() {
     if Appodeal.isReadyForShowWithStyle(.Interstitial) {
        Appodeal.showAd(.Interstitial, rootViewController: self)
     }
    }
    func viewWasUnhidden(){
        
        appWasUnhidden += 1
        print(appWasUnhidden)
        
        //check if appWasUnhidden 3 times and show ad
        
        if appWasUnhidden % 2 == 0 {
            showAd()
        }
    }
*/
    func showRateAlert() {
        let neverRate = UserDefaults.standard.bool(forKey: "neverRate")
        var numLaunches = UserDefaults.standard.integer(forKey: "numLaunches") + 1
        
        if (!neverRate && numLaunches == minimumSessions || numLaunches >= (minimumSessions + tryAgaingAfter_Launches)) {
            makeAlertView()
            numLaunches = minimumSessions + 1
        }
        UserDefaults.standard.set(numLaunches, forKey: "numLaunches")
        
        
    }
    
    func makeAlertView() {
        let alertView = UIAlertController(title: "Оставьте отзыв!", message: "Спасибо за использование нашего калькулятора!\nПоставьте оценку в App Store и напишите отзыв.\nВаше мнение важно для нас!", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Ок", style: .default, handler: {alertAction in
            UIApplication.shared.openURL(URL(string : "itms-apps://itunes.apple.com/app/id1132846669")!)
            alertView.dismiss(animated: true, completion: nil)
        }))
        alertView.addAction(UIAlertAction(title: "Никогда", style: .default, handler: {alertAction in
            UserDefaults.standard.set(true, forKey: "neverRate")
            alertView.dismiss(animated: true, completion: nil)
        }))
        alertView.addAction(UIAlertAction(title: "Не сейчас", style: .default, handler: {alertAction in
            alertView.dismiss(animated: true, completion: nil)
        }))
        self.present(alertView, animated: true, completion: nil)
    }
    

}
extension ViewController: GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!,
                present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
                dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}
