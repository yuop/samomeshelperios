//
//  SplashViewController.swift
//  SamomesHelper
//
//  Created by Savelyev Yura on 27/06/2016.
//  Copyright © 2016 Savelyev Yura. All rights reserved.
//

import UIKit
import KYCircularProgress
class SplashViewController: UIViewController {

    fileprivate var phrases = ["Чистим бутылочки", "Проверяем аромки", "Ищем никотин", "Напрягаем процессоры", "Запасаемся салфетками", "Запускаем калькулятор"]
    fileprivate var progress:UInt8 = 0
    
    fileprivate var circularProgress: KYCircularProgress!
    fileprivate var timer : Timer!
    let redColor = UIColor(red: 212/255, green: 0, blue: 0, alpha: 1)
    let greenColor = UIColor(red:  0, green: 199/255, blue: 0, alpha: 1)
    let blueColor = UIColor(red: 0, green: 168/255, blue: 1, alpha: 1)
    let whiteColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createProgressBar()

        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(SplashViewController.updateProgress), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func createProgressBar(){
        let width = view.frame.width
        let height = view.frame.height
        let circularProgressFrame = CGRect(x: 0, y: 0, width: width, height: height/1.5)
        circularProgress = KYCircularProgress(frame: circularProgressFrame, showGuide: true)
        
        let center = CGPoint(x: width/2, y: height/2)
        
        circularProgress.path = UIBezierPath(arcCenter: center, radius: CGFloat(circularProgress.frame.width/3), startAngle: CGFloat(M_PI), endAngle: CGFloat(0.0), clockwise: true)
        
        circularProgress.colors = [greenColor, blueColor]
        circularProgress.lineWidth = 8.0
        circularProgress.guideColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 0.4)
        
        let textLabel = UILabel(frame: CGRect(x: circularProgressFrame.width/2 - 125, y: 400, width: 250.0, height: 32.0))
        textLabel.font = UIFont(name: "Raleway", size: 20)
        textLabel.textAlignment = .center
        textLabel.textColor = whiteColor
        textLabel.alpha = 0.3
        textLabel.text = self.phrases.first
        circularProgress.addSubview(textLabel)
        
        
        
        circularProgress.progressChanged(completion: { (progress: Double,circularView:KYCircularProgress) in
            
            if progress.truncatingRemainder(dividingBy: 0.2) == 0 {
                
                let rnd = Int(arc4random_uniform(UInt32(self.phrases.count - 1)))
                textLabel.text = self.phrases.remove(at: rnd)
                
                if progress == 0.8 {
                    textLabel.text = self.phrases.last
                }
                
            } else if progress == 1 {
                self.performSegue(withIdentifier: "fromSplash", sender: nil)
            
            }
        })
        
        view.addSubview(circularProgress)

    }
    
    func updateProgress() {
        progress = progress &+ 1
        let normProgress = Double(progress) / 255.0
        circularProgress.progress = normProgress
        if normProgress == 1 {
            timer.invalidate()
            timer = nil
        }
        
    }

    
    /// MARK: - Navigation

 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mainStoryboard = UIStoryboard(name: "MyStoryboard", bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "samomesController")
        UIApplication.shared.keyWindow?.rootViewController = mainViewController
    }

}
